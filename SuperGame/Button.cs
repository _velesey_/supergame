﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Color = Microsoft.Xna.Framework.Color;
using Rectangle = Microsoft.Xna.Framework.Rectangle;

public class Button : DrawableGameComponent
{
    // Текстуры кнопки: ненажатое состояние, при наведении курсора, при нажатии.
    private readonly Texture2D[] _buttonTexture;
    private readonly ContentManager _content;
    private readonly SpriteBatch _spriteBatch;
    private readonly string _text;
    private string ImageName;
    private float Transparency = 1;

    // Ограничивающий прямоугольник. Его размер равен размеру текстуры.
    private Rectangle _boundingBox;
    // Состояние кнопки: ненажатое состояние, курсор наведён, нажата.
    public State _state;
    // Текстура текста кнопки.
    private Texture2D _textSprite;

    /// <summary>
    ///   Кнопка.
    /// </summary>
    /// <param name = "game">Текущая игра.</param>
    /// <param name = "x">Координата X.</param>
    /// <param name = "y">Координата Y.</param>
    /// <param name = "text">Текст кнопки. Не обязателен.</param>
    public Button(Game game, int x, int y, int width, int heigth, string imageName, float transparency, string text = null)
        : base(game)
    {
        _text = text;
        ImageName = imageName;
        Transparency = transparency;
        _content = new ContentManager(game.Services) { RootDirectory = "Content" };
        _spriteBatch = new SpriteBatch(game.GraphicsDevice);
        _buttonTexture = new Texture2D[3];
        _boundingBox = new Rectangle(x, y, width, heigth);
    }

    /// <summary>
    ///   Действие, выполняемое при нажатии кнопки.
    /// </summary>
    public Action Action { get; set; }

    public override void Initialize()
    {
        // Начальное состояние.
        _state = State.BTN_NORMAL;
        base.Initialize();
    }

    public override void Update(GameTime gameTime)
    {
        // Получаем состояние мыши.
        var mouseState = Mouse.GetState();

        // Проверяем, находится ли курсор внутри кнопки.
        if (_boundingBox.Contains(mouseState.X, mouseState.Y))
            // Проверяем, нажата ли кнопка мыши.
            if (mouseState.LeftButton == ButtonState.Pressed)
            {
                // Выполняем действие. 
                // Благодаря проверке _state == State.BTN_HOVER действие выполняется один раз

                //if (Action != null && _state == State.BTN_HOVER)
                    Action.Invoke();
                // Меняем состояние.
                _state = State.BTN_PRESSED;
            }
            else
                _state = State.BTN_HOVER;
        else
            _state = State.BTN_NORMAL;

        base.Update(gameTime);
    }

    public override void Draw(GameTime gameTime)
    {
        _spriteBatch.Begin();
        // Рисуем текстуру кнопки соответствующую состоянию.
        _spriteBatch.Draw(_buttonTexture[(int)_state], _boundingBox, Color.White * Transparency);
        // Если задан текст, то отрисовываем его.
        if (!string.IsNullOrWhiteSpace(_text))
            _spriteBatch.Draw(_textSprite, _boundingBox, Color.White);
        _spriteBatch.End();
    }

    protected override void LoadContent()
    {
        // Загружаем текстуры кнопки.
        _buttonTexture[0] = _content.Load<Texture2D>(ImageName);
        _buttonTexture[1] = _content.Load<Texture2D>(ImageName);
        _buttonTexture[2] = _content.Load<Texture2D>(ImageName);

        // Задаём ограничивающий прямоугольник.
        _boundingBox.Width = _buttonTexture[0].Width;
        _boundingBox.Height = _buttonTexture[0].Height;

        // Если задан текст, создаём текстуру для отрисовки текста.
        if (!string.IsNullOrWhiteSpace(_text))
            CreateTextSprite();
    }

    // Создание текстуры текста. Создаём через средства GDI+. 
    // Это позволяем отцентрировать текст внутри кнопки и нормально его сгладить.
    private void CreateTextSprite()
    {
        // Битовая текстура.
        var bitmap = new Bitmap(_boundingBox.Width, _boundingBox.Height);
        // Чем будем рисовать.
        var g = Graphics.FromImage(bitmap);
        // Устанавливаем сглаживание текста.
        g.TextRenderingHint = TextRenderingHint.AntiAlias;
        // Создаём шрифт.
        var fontFamily = new FontFamily("Arial");
        var font = new Font(fontFamily, 12, FontStyle.Regular, GraphicsUnit.Pixel);
        // Формат строки. Располагаем текст в центре кнопки.
        var stringFormat = new StringFormat
        {
            Alignment = StringAlignment.Center,
            LineAlignment = StringAlignment.Center
        };
        // Отступ от границ кнопки.
        const int BORDER = 4;
        // Создаём прямоугольник, в котором будет отрисовывать текст.
        var surroundingRectangle = new RectangleF(BORDER, BORDER,
                                                  _boundingBox.Width - 2 * BORDER,
                                                  _boundingBox.Height - 2 * BORDER);
        // Рисуем текст.
        g.DrawString(_text, font, Brushes.Black, surroundingRectangle, stringFormat);
        // Создаём из битовой текстуры Texture2D 
        using (var stream = new MemoryStream())
        {
            bitmap.Save(stream, ImageFormat.Png);
            stream.Seek(0, SeekOrigin.Begin);
            _textSprite = Texture2D.FromStream(GraphicsDevice, stream);
        }
    }

    #region Nested type: State

    public enum State
    {
        BTN_NORMAL = 0,
        BTN_HOVER,
        BTN_PRESSED,
    }

    #endregion
}