﻿#region Using Statements
	using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.GamerServices;
using System.IO;
#endregion

namespace SuperGame
{
	/// <summary>
	/// This is the main type for your game
	/// </summary>
	public class Game1 : Game
	{
		GraphicsDeviceManager graphics;
		SpriteBatch spriteBatch;
        private Button btn_up;
	    private Button btn_down;
	    private Button btn_left;
	    private Button btn_rigth;
	    private Button btn_jump;

	    private bool Is_up;
	    private bool Is_down;
	    private bool Is_left;
	    private bool Is_rigth;
	    private bool Is_jump;

		public Hero hero;
	    public bool HeroIsDead;

		public List<Block> blockList;
        private SoundEffectInstance engineSoundInstance;
		Texture2D blockTexture;
        public int width;
        public int heigth;
		public Map map;

        int ScrollX;
        int levelLength;
		public string GameTitle = "SuperGame";
		Dictionary<long, string> songDict;
		public List<Texture2D> MelList;
		private CatcherHolder m_cHolder;

		public int scores = 0; // î÷êè
		public float self_size = 1f; // ðàçìåð "èãðîêà"
		public int xsize = 1; // ìíîæèòåëü î÷êîâ

		public float power = 0f; // ïåðåìåííàÿ äëÿ ïóðïóðíîãî áîíóñà
		public float activity = 0f; // ïåðåìåííàÿ äëÿ àêòèâíîñòè èãðîêà

		private SoundEffect song;



		public Game1()
			: base()
		{
			graphics = new GraphicsDeviceManager(this);
			Content.RootDirectory = "Content";

			this.Components.Add (new Fps (this));
			hero = new Hero(new Rectangle(10,10,71,91), this);
		    HeroIsDead = false;
		}

		/// <summary>
		/// Allows the game to perform any initialization it needs to before starting to run.
		/// This is where it can query for any required services and load any non-graphic
		/// related content.  Calling base.Initialize will enumerate through any components
		/// and initialize them as well.
		/// </summary>
		protected override void Initialize()
		{
			// TODO: Add your initialization logic here
			IsFixedTimeStep = true;
			graphics.SynchronizeWithVerticalRetrace = false;

            //width = graphics.PreferredBackBufferWidth = 800;
            //heigth = graphics.PreferredBackBufferHeight = 600;

            width = graphics.PreferredBackBufferWidth = GraphicsDevice.DisplayMode.Width;
            heigth = graphics.PreferredBackBufferHeight = GraphicsDevice.DisplayMode.Height;
            graphics.IsFullScreen = true;
            graphics.ApplyChanges();

            base.Initialize();
		
			IsMouseVisible = false;

		}

		/// <summary>
		/// LoadContent will be called once per game and is the place to load
		/// all of your content.
		/// </summary>
		protected override void LoadContent()
		{
			// Create a new SpriteBatch, which can be used to draw textures.
			spriteBatch = new SpriteBatch(GraphicsDevice);
			//hero.SpriteLeft = Content.Load<Texture2D>("hero_left"); 
			hero.SpriteRight = Content.Load<Texture2D>("hero_right");  
			//hero.SpriteStay = Content.Load<Texture2D>("hero"); 
			//hero.SpriteJump = Content.Load<Texture2D>("hero_jump"); 

			Block.footerTexture = Content.Load<Texture2D>("block");
			Block.texturePlatformList.Add(Content.Load<Texture2D>("block"));
			Block.texturePlatformList.Add(Content.Load<Texture2D>("block_n_1"));
			Block.texturePlatformList.Add(Content.Load<Texture2D>("block_n_2"));
			Block.texturePlatformList.Add(Content.Load<Texture2D>("block_n_3"));
			Block.texturePlatformList.Add(Content.Load<Texture2D>("block_n_4"));


			Block.textureDeathPlatformList.Add(Content.Load<Texture2D>("block attack 1"));
			Block.textureDeathPlatformList.Add(Content.Load<Texture2D>("block attack 2"));
			Block.textureDeathPlatformList.Add(Content.Load<Texture2D>("block attack 3"));
			Block.textureDeathPlatformList.Add(Content.Load<Texture2D>("block attack 4"));

            InitializeAndDrowButtons();

			blockList =  new List<Block>();

			var list  = GeneratorBlocks.ReadFile(Path.Combine(Content.RootDirectory, "song.txt"));
			songDict = GeneratorBlocks.ParseList (list);

			Dictionary<long, int> newSongDict = new Dictionary<long, int> ();

			foreach (var d in songDict) {
				int cnt = 0;
				foreach(var v in d.Value){
					if ("1".Contains(v.ToString()))
						cnt++;
				}
				newSongDict.Add (d.Key, cnt);
			}

			map = new Map (width, heigth, newSongDict);


			MelList = new List<Texture2D>();

			for (int a = 1; a <= 5; a++)
				MelList.Add(Content.Load<Texture2D>("mel" + a));

			m_cHolder = new CatcherHolder(MelList);

			song = Content.Load<SoundEffect>("mario song");

		    engineSoundInstance = song.CreateInstance();
            engineSoundInstance.Play();

		}


	    private void InitializeAndDrowButtons()
	    {
            btn_down = new Button(this, (int)(width * 0.9), (int)(heigth * 0.9), 10, 10, "down", 0.5f);
            btn_down.Action += () =>
            {
                if (btn_down._state == Button.State.BTN_PRESSED)
                {
                    Is_down = true;
                    Is_jump = false;
                    Is_left = false;
                    Is_rigth = false;
                    Is_up = false;
                }
            };
            // Регистрируем кнопку.
            Components.Add(btn_down);
            btn_down.Initialize();

            btn_up = new Button(this, (int)(width * 0.9), (int)(heigth * 0.7), 10, 10, "up", 0.5f);
	        btn_up.Action += () =>
	        {
                if (btn_up._state == Button.State.BTN_PRESSED)
                {
                    Is_down = false;
                    Is_jump = false;
                    Is_left = false;
                    Is_rigth = false;
                    Is_up = true;
                }
	        };
            Components.Add(btn_up);
            btn_up.Initialize();

            btn_left = new Button(this, (int)(width/20), (int)(heigth * 0.8), 10, 10, "left", 0.5f);
            btn_left.Action += () =>
            {
                if (btn_left._state == Button.State.BTN_PRESSED)
                {
                    Is_down = false;
                    Is_jump = false;
                    Is_left = true;
                    Is_rigth = false;
                    Is_up = false;
                }
            };
            Components.Add(btn_left);
            btn_left.Initialize();

            btn_rigth = new Button(this, width/5, (int)(heigth * 0.8), 10, 10, "rigth", 0.5f);
            btn_rigth.Action += () =>
            {
                if (btn_rigth._state == Button.State.BTN_PRESSED)
                {
                    Is_down = false;
                    Is_jump = false;
                    Is_left = false;
                    Is_rigth = true;
                    Is_up = false;
                }
            };
            Components.Add(btn_rigth);
            btn_rigth.Initialize();
	    }

		/// <summary>
		/// UnloadContent will be called once per game and is the place to unload
		/// all content.
		/// </summary>
		protected override void UnloadContent()
		{
			// TODO: Unload any non ContentManager content here
		}

		/// <summary>
		/// Allows the game to run logic such as updating the world,
		/// checking for collisions, gathering input, and playing audio.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Update(GameTime gameTime)
		{
			if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
				Exit();

			KeyboardState states = Keyboard.GetState();

			hero.SetAnimatedState (Global.HeroAnimatedState.stay);

			if (states.IsKeyDown (Keys.Up) || Is_up) {
				hero.jump (gameTime);
			}

			if (states.IsKeyDown (Keys.Left) || Is_left) {
				hero.SetAnimatedState (Global.HeroAnimatedState.left);
			}
			if (states.IsKeyDown (Keys.Right) || Is_rigth) {
				hero.SetAnimatedState (Global.HeroAnimatedState.right);
			}



			m_cHolder.Update (this);


            Is_down = false;
            Is_jump = false;
            Is_left = false;
            Is_rigth = false;
            Is_up = false;

			hero.Update (gameTime);

		    
			// TODO: Add your update logic here

			base.Update(gameTime);

            if (hero.isDeath)
            {
                HeroIsDead = true;
                engineSoundInstance.Stop();
                Exit();
            }
		}

		/// <summary>
		/// This is called when the game should draw itself.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Draw(GameTime gameTime)
		{

			GraphicsDevice.Clear(Color.TransparentBlack);

			spriteBatch.Begin();

			hero.Draw (spriteBatch, gameTime);

			map.Draw (gameTime, spriteBatch);





			spriteBatch.End ();

			m_cHolder.Draw(spriteBatch);


			base.Draw(gameTime);
		}
	}
}
