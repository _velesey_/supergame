using System;

namespace SuperGame
{
	public static class Global
	{
		public 	enum HeroAnimatedState  {
			stay = 0, 
			left = 1,
			right = 2,
			jump = 3
		}

		public enum BlockType {
			platform = 1,
			death = 2,
			footer = 3
		}
	}
}

