using System;
using System.Diagnostics;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;


namespace SuperGame
{
	public class Hero
	{
		public Texture2D SpriteStay;

		public Texture2D SpriteLeft;

		public Texture2D SpriteRight;

		public Texture2D SpriteJump;

		public Rectangle Position;

		int FrameCount = 10; //Количество всех фреймов в изображении (у нас это 10)
		int frame;//какой фрейм нарисован в данный момент
		float TimeForFrame;//Сколько времени нужно показывать один фрейм (скорость)
		float TotalTime;//сколько времени прошло с показа предыдущего фрейма 
		int speedAnimation  = 10;
		int startSpeedAnimation  = 10;

		float speedRun = 0.4f;
		float jumpSpeed = 0.5f;
		float maxJumpSpped = 9;
		float g = 0.8f;

		public bool isDeath = false;

		Global.HeroAnimatedState state = Global.HeroAnimatedState.left;

		Game1 game;
		bool isJumped = false;

		public Hero (Rectangle position, Game1 game)
		{
			Position = position;
			this.game = game;
			frame = 0;
			TimeForFrame = (float)1 / speedAnimation;
			TotalTime = 0; 

		}


		public void SetAnimatedState(Global.HeroAnimatedState state){

			this.state = state;
		}

		public void Update(GameTime gameTime)
		{
			if (gameTime.TotalGameTime.Seconds>8)
				if (Position.Y > game.heigth + 40)
					isDeath = true;



			if (isCanDown(gameTime) || jumpSpeed > 0)
			Gravity (gameTime);

			speedAnimation = startSpeedAnimation;
			if (state == Global.HeroAnimatedState.left) {
				MoveLeft (gameTime);
				speedAnimation = 7;
			} else if (state == Global.HeroAnimatedState.right) {
				speedAnimation = 20;
				MoveRight (gameTime);

			} 
			Rectangle nextPosition = Position;

			nextPosition.Offset((int)(speedRun * gameTime.ElapsedGameTime.Milliseconds),0);

			foreach (var group in game.map.groups){
				foreach(var block in group.blocks)
					if (nextPosition.Intersects(block.Rect)){
						if (block.blockType == Global.BlockType.death)
							isDeath = true;
					else
						MoveLeft (gameTime);
				}
			}

			if (state == Global.HeroAnimatedState.jump) {
				jump (gameTime);
			}
			TimeForFrame = (float)1 / speedAnimation;

			Texture2D texture = GetTextureByState ();


				FrameCount = texture.Width / texture.Height;

				TotalTime += (float)gameTime.ElapsedGameTime.TotalSeconds;

				if (TotalTime > TimeForFrame) {
					frame++;

					frame = frame % (FrameCount - 1);

					TotalTime -= TimeForFrame;
			}
		} 

	
		private bool isCanDown(GameTime gametime){

		Rectangle nextPosition = Position;
			nextPosition.Offset (0, (int)(-jumpSpeed  * gametime.ElapsedGameTime.Milliseconds / 10));

			foreach (var group in game.map.groups){
				foreach(var block in group.blocks)
				if (nextPosition.Intersects(block.Rect)){
					return false;
				}
			}
			return true;
				 
		}
	
		private Texture2D GetTextureByState()
		{

			return SpriteRight;
		}

		private void Gravity(GameTime gametime){
		    
			jumpSpeed = jumpSpeed - g; 
			Position.Offset (0, (int)(-jumpSpeed  * gametime.ElapsedGameTime.Milliseconds / 10));
		//	System.Console.WriteLine (Position.Top);

		}

		private void MoveLeft(GameTime gametime){
//			Rectangle nextPosition = Position;
//			nextPosition.Offset((int)(-speedRun * gametime.ElapsedGameTime.Milliseconds),0);
//			foreach (var block in game.blockList) {
//				if (nextPosition.Intersects (block.Rect)) {
//					MoveRight (gametime);
//
//				}
//			}
			Position.Offset((int)(-speedRun * gametime.ElapsedGameTime.Milliseconds),0);

		}
		private void MoveRight(GameTime gametime){
		
			Position.Offset((int)(speedRun * gametime.ElapsedGameTime.Milliseconds), 0);
		}

		public void jump(GameTime gametime)
		{
			if (!isCanDown(gametime))
			jumpSpeed = maxJumpSpped;
		}

		public void Draw(SpriteBatch batch, GameTime gametime){
		
			Texture2D texture = GetTextureByState ();

			int frameWidth = 710 / 10;
			Rectangle rectanglе = new Rectangle(frameWidth * frame, 0, 71, 91); 
			//Rectangle rectanglе = new Rectangle(frameWidth * frame, 0, 71, 91); 

			batch.Draw (texture, new Vector2(Position.X, Position.Y), rectanglе, Color.White); 

				//System.Console.WriteLine (frameWidth + "" + texture.Height);
			}

		}
	}


