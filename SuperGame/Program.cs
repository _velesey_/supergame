﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
#endregion

namespace SuperGame
{
#if WINDOWS || LINUX
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            bool newGame;
            bool exitGame;
            bool gameOver;

            using (var menu = new Menu())
            {
                menu.Run();
                newGame = menu.NewGame;
                exitGame = menu.Exit;
            }

            using (var game = new Game1())
            {
                if (newGame && !exitGame) game.Run();
                gameOver = game.HeroIsDead;
            }
            if (gameOver)
            {
                using (var gameOver1 = new GameOver())
                {
                    gameOver1.Run();
                }
            }
        }
    }
#endif
}
