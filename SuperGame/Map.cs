﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperGame
{
    public class Map
    {
        int width;
        int heigth;
        Texture2D blockTexture;
        public List<Group> groups;
        public int blockSize;
        public int groupWidth;
        public int groupHeigth;
        public int groupCount;
        public float speed = 0.2f;
        Dictionary<long, int> templates;

		public Map(int width, int heigth, Dictionary<long, int> templs )
        {
            this.width = width;
            this.heigth = heigth;
            this.blockTexture = blockTexture;
            blockSize = 32;
            groupWidth = blockSize * 5;
            groupHeigth = blockSize * 6;

			templates = templs;
//            templates.Add(1, 1);
//            templates.Add(2, 2);
//            templates.Add(3, 3);
//            templates.Add(4, 4);
//			templates.Add(5, 5);
//            templates.Add(6, 1);

            createMap();
        }

        private void createMap()
        {
            groups = new List<Group>();
            groupCount = width / groupWidth + 2;

            int x = 0;
            int y = heigth - groupHeigth;

            for (int i = 0; i < groupCount; i++)
            {
                Group group = new Group(blockTexture, x, y, blockSize);
                groups.Add(group);
                x += groupWidth;
            }
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            foreach (var group in groups)
            {
                group.Draw(gameTime, spriteBatch, offset(gameTime, group), 0);
            }
        }

        private int offset(GameTime gameTime, Group group)
        {
            int offset = (int)(speed * gameTime.ElapsedGameTime.Milliseconds);
            if (group.StartX + groupWidth - offset < 0)
            {
                int time = (int) (gameTime.TotalGameTime.TotalSeconds);
                int template = 0;
                templates.TryGetValue(time, out template);
                group.Reset(template);
                return groupWidth * groupCount - offset; 
            }
            return -offset;
        }
    }
}
