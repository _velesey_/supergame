using System;
using Microsoft.Xna.Framework;

namespace SuperGame
{
	public class Fps : DrawableGameComponent
	{
		public int FPS;
		private int frames;
		private double seconds;

		public Fps (Game game) : base(game)
		{
		}

		public override void Update (GameTime gameTime)
		{
			seconds += gameTime.ElapsedGameTime.TotalSeconds;
			if (seconds >= 1) {
				FPS = frames;
				seconds = frames = 0;
				Game.Window.Title = "FPS = " + FPS.ToString ();
			}
			base.Update (gameTime);
		}

		public override void Draw (GameTime gameTime)
		{
			frames++;
			base.Draw (gameTime);
		}
	}
}

