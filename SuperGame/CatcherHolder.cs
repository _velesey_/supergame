﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace SuperGame
{
    public class CatcherHolder
    {
        private Random random; // Генератор случайных чисел

        public List<Catcher> particles; // Массив частичек (Catcher)
        private List<Texture2D> textures; // Массив текстур

        public List<float> accomulator { get; set; } // Массив float-значений, что такое и зачем нужен accomulator — объясню чуть позже.

        public CatcherHolder(List<Texture2D> textures)
        {
            this.textures = textures;
            this.particles = new List<Catcher>();

            random = new Random();

            accomulator = new List<float>(); // Инициализируем массив и записываем во все 128 ячеек — 1.0f
            for (int a = 0; a < Constanst.MilsCount; a++)
            {
                accomulator.Add(1.0f);
            }
        }
    // Генерация одной частички
    // Wave - волна, число от 0f до ширины экрана.
     private Catcher GenerateNewParticle(float Wave) 
        {
            Texture2D texture = textures[random.Next(textures.Count)]; // Берем случайную текстуру из массива
            Vector2 position = new Vector2(Wave, 0); // Задаем позицию
            Vector2 velocity = new Vector2((float)(random.NextDouble()  - 0.5), (float)(random.NextDouble() * 10)); // Случайное ускорение, 0.5f для X и 10f для Y
            float angle = 0; // Угол поворота = 0
            float angularVelocity = 0.05f * (float)(random.NextDouble()*2 - 1 ); // Случайная скорость вращения
            Color color = new Color(0f, 1f, 0f); // Зеленый цвет (изменится цвет уже в самом Catcher)
            float size = (float)random.NextDouble()*.8f + .2f; // Случайный размер
            int ttl = 600; // Время жизни в 400 (400 актов рисования живет частица, т.е. 400 / 60 — 6 с лишним секунд.

            int type = 0; //— изначальный тип 0

        // Вероятность появления
         if (random.Next(10000) > 9900) // враг
                type = 1;
            else if (random.Next(10000) > 9950) // желтый
                type = 3;
            else if (random.Next(10000) > 9997) // пурпурный
                type = 2;
            else if (random.Next(10000) > 9998) // мигающий
                type = 4;
    
            return new Catcher(texture, position, velocity, angle, angularVelocity, type, size, ttl); // Создаем частичку и возвращаем её
        }

    // Генерация желтых частичек при касании с красной частичкой
     public void GenerateYellowExplossion(int x, int y, int radius)
        {
            Texture2D texture = textures[random.Next(textures.Count)];

            Vector2 direction = Vector2.Zero;
            float angle = (float)Math.PI * 2.0f * (float)random.NextDouble();
            float length = radius * 4f;

            direction.X = (float)Math.Cos(angle);
            direction.Y = -(float)Math.Sin(angle);

            Vector2 position = new Vector2(x, y) + direction * length;

            Vector2 velocity = direction * 4f;
            
            float angularVelocity = 0.05f * (float)(random.NextDouble() * 2 - 1);

            float size = (float)random.NextDouble() * .8f + .2f;
            int ttl = 400;

            int type = 3;
            particles.Add(new Catcher(texture, position, velocity, 0, angularVelocity, type, size, ttl));
        }

    // "Музыкальный" импульс, создание частички
        public void Beat(float Wave)
        {
            particles.Add(GenerateNewParticle(Wave));
        }

        public void Update(Game1 game) // Обновление всех частиц
        {

            //////////

            // "Прогоняем" массив с частотами, выполняем условия
//            for (int a = 0; a < Constanst.MilsCount; a++)
//            {
//                if (musicGeneration.visualizationData.Frequencies[a] > Constanst.BEAT_REACTION && accomulator[a] > Constanst.ACCOMULATOR_REACTION)
//                {
//                    Beat(a * 3.125f * 2); // вызываем "бит", которые создает частичку.
//                    accomulator[a] -= Constanst.BEAT_COST; // убавляем аккумулятор
//                }
//            }
			Random rnd = new Random ();
			Beat(rnd.Next(1,100)* 3.125f * 2);
            // проверяем, есть ли бонус, который тянет к игроку все ноты
           
                for (int particle = 0; particle < particles.Count; particle++)
                {
                   
                        float body1X = particles[particle].Position.X;
                        float body1Y = particles[particle].Position.Y;
			     		float body2X = (float)game.hero.Position.X;
						float body2Y = (float)game.hero.Position.Y;

                        float Angle = (float)Math.Atan2(body2X - body1X, body2Y - body1Y) - ((float)Math.PI / 2.0f); // находим угол к игроку

                        float Lenght = (float)(300f) / (float)Math.Pow((float)Distance(body1X, body1Y, body2X, body2Y), 2.0f); // находим силу

                        particles[particle].ApplyImpulse(AngleToV2(Angle, Lenght)); // даем пинка ноте
                    }
               // game.power -= 0.001f; // убавляем бонус

        //    game.activity -= 0.001f; // убавляем активность игрока

//            if (game.activity < 0.0f)
  //              game.activity = 0.0f;
    //        else if (game.activity > 0.5f) game.activity = 0.5f;																																														
            // Держим активность игрока от 0f до .5f

            // Проверяем столкновения двух кругов: игрока и нот
            for (int particle = 0; particle < particles.Count; particle++)
            {
                int x = (int)particles[particle].Position.X;
                int y = (int)particles[particle].Position.Y;
                int radius = (int)(16f * particles[particle].Size);

                if (circlesColliding(Mouse.GetState().X, Mouse.GetState().Y, (int)(16f * game.self_size), x, y, radius))
                {

                    game.scores += (int)(10f * particles[particle].Size * game.xsize); // добавляем очки, которые зависят от размера ноты и множителя
              //      game.activity += 0.005f; // добавляем активность
                    int type = particles[particle].type;

                    // выполняем всякие условия, которые возникают при коллизии
                    switch (type)
                    {
                        case 3: // желтый
                            game.self_size += 0.1f;
                            game.xsize += 1;

                            // увеличиваем множитель и размер игрока
                            if (game.self_size > 4.0f)
                                game.self_size = 4.0f;
                            break;

                        case 2: // пурпурный
                            game.power = 1f; // даем бонус игроку, который все ноты притягивает к себе

                            break;

                        case 4: // мигающий
                            for (int b = 0; b < particles.Count; b++)
                                particles[b].SetType(3); // устанавливает всем нотам тип — желтый


                            break;

                        case 1: // красный (враг)
                            for (int a = 1; a < game.xsize; a++)
                                GenerateYellowExplossion(Mouse.GetState().X, Mouse.GetState().Y, (int)(16f * game.self_size));

                            game.xsize = 1;
                            game.self_size = 1f;
                            game.scores -= (int)(game.scores / 4);
                            break;

                    }


                    // удаляем частичку
                    particles[particle].TTL = 0;
                    particles.RemoveAt(particle);
                    particle--;
                }
            }
           
            /////////
           
           for (int particle = 0; particle < particles.Count; particle++)
            {
                particles[particle].Update();
                if (particles[particle].Size <= 0 || particles[particle].TTL <= 0)
                {
            // Если частичка дохлая или размер нуль или меньше, удаляем её
                    particles.RemoveAt(particle);
                    particle--;
                }
            }
    
    // Обновляем аккумулятор, если значения ячейки меньше 1f, то добавляем значение, указанное в статическом классе Constants — ACCUMULATE_SPEED, листинг Constanst - ниже.
            for (int a = 0; a < Constanst.MilsCount; a++)
                if (accomulator[a] < 1.0f)
                    accomulator[a] += Constanst.ACCUMULATE_SPEED; 
            
        }

        public void Draw(SpriteBatch spriteBatch)
        {
        // Прорисовываем все частички, важно указать BlendState.Additive, чтобы частички были более "мягкие".
            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.Additive);
            for (int index = 0; index < particles.Count; index++)
            {
                particles[index].Draw(spriteBatch);
            }
            spriteBatch.End();
        }

        // дистанция
        public float Distance(float x1, float y1, float x2, float y2)
        {
            return (float)Math.Sqrt((float)Math.Pow(x2 - x1, 2) + (float)Math.Pow(y2 - y1, 2));
        }

        // возвращает, столкнулись ли два круга или нет
        bool circlesColliding(int x1, int y1, int radius1, int x2, int y2, int radius2)
        {

            int dx = x2 - x1;
            int dy = y2 - y1;
            int radii = radius1 + radius2;
            if ((dx * dx) + (dy * dy) < radii * radii)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        // функция перевода угла в вектор
        public Vector2 AngleToV2(float angle, float length)
        {
            Vector2 direction = Vector2.Zero;
            direction.X = (float)Math.Cos(angle) * length;
            direction.Y = -(float)Math.Sin(angle) * length;
            return direction;
        }
    }
}