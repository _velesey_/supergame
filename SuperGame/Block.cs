using System;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.Collections.Generic;


namespace SuperGame
{

	public class Block
	{
        private Rectangle _rect;
		private Texture2D texture; 
        public static Texture2D footerTexture; 

		public Global.BlockType blockType;

		public static  List<Texture2D> texturePlatformList = new List<Texture2D>();
		public static List<Texture2D> textureDeathPlatformList = new List<Texture2D>();
        
        public Rectangle Rect 
        {
            get { return _rect; }
        }

        public Block(Texture2D text, Rectangle rect, Global.BlockType blocktype)
        {
			this.texture = text;
            this._rect = rect;
			blockType = blocktype;
		}

        public void Update(GameTime gameTime)
        {
		
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch, int dx, int dy)
        {
			if (texture == null) {
				texture = Block.footerTexture;

				if (blockType == Global.BlockType.platform) {
					Random rnd = new Random ();
					texture = Block.texturePlatformList [rnd.Next (0, Block.texturePlatformList.Count - 1)];
				} 
				if (blockType == Global.BlockType.death) {
					Random rnd = new Random ();
					texture = Block.textureDeathPlatformList [rnd.Next (0, Block.textureDeathPlatformList.Count - 1)];
				} 
			}
            if (dx != 0 || dy != 0)
            {
                _rect.Offset(dx, dy);
            }
			try
			{
            spriteBatch.Draw(texture, _rect, Color.White);
			}
			catch{
			}
		}

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            Draw(gameTime, spriteBatch, 0, 0);
        }
	}
}

