using System;
using System.Collections.Generic;
using System.IO;
using System.Data.Linq;
using System.Linq;

namespace SuperGame
{
	public class GeneratorBlocks
	{
		static long timeInterval = 30; //секунда * 10
		static long timeSubInterval = 6; //секунда * 10

		public GeneratorBlocks ()
		{

		}

		public static List<long> ReadFile(string filename){
			var stream = File.OpenText (filename);

			var list = new List<long> ();
			string line = "";
			while ((line = stream.ReadLine()) != null) {
				list.Add (long.Parse (line));

			}
			return list;
		}

		public static Dictionary<long, string> ParseList(List<long> list)
		{
			Dictionary<long, string> dict = new Dictionary<long, string> ();

			for (long i = 0; i < list[list.Count - 1]; i+=timeInterval) {

				var subList = (from x in list
				           where x > i && x < i + timeInterval
				           select x).ToList();

				List<short> blockList = new List<short> ();
				for (long si = i; si < subList[subList.Count - 1]; si+=timeSubInterval) {

					int cnt = (from x in subList
					           where x > si && x < si + timeSubInterval
					           select x).Count();

					if (cnt > 0) 
						blockList.Add (1);
					else
						blockList.Add (0);
				}
			
				string num = "";
				blockList.ForEach(x=> num += x.ToString());

				long average = (long)(from x in list
				                      where x > i && x < i + timeInterval
				                      select x).Average();
				average = (int)(average / 10);
				dict.Add (average, num);
			}

			return dict;

		}

	}
}

