﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
#endregion

namespace SuperGame
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Menu : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        public string GameTitle = "Menu";
        private Texture2D backgroundTexture2D;
        private SoundEffect matrixThemeSong;
        private SoundEffectInstance engineSoundInstance;  
        private Rectangle _rectangle;
        private int heigth;
        private int width;
        public bool NewGame;
        public bool Exit;
        private SongCollection _songCollection;

        public Menu()
            : base()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            this.Components.Add(new Fps(this));
            heigth = graphics.PreferredBackBufferHeight;
            width = graphics.PreferredBackBufferWidth;
            _rectangle = new Rectangle(0, 0,width, heigth);
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            IsFixedTimeStep = false;
            graphics.SynchronizeWithVerticalRetrace = false;
            IsMouseVisible = true;

            var btn_newGame = new Button(this, 20, 150, 100, 100, "MenuNewGame", 1);
            btn_newGame.Action += () =>
            {
                NewGame = true;
                Exit = false;
                engineSoundInstance.Stop();
                this.Exit();
            };
            // Регистрируем кнопку.
            Components.Add(btn_newGame);

            var btn_exit = new Button(this, 450, 150, 100, 100, "MenuExit", 1);
            btn_exit.Action += () =>
            {
                NewGame = false;
                Exit = true;
                //engineSoundInstance.Stop();
                this.Exit();
            };
            // Регистрируем кнопку.
            Components.Add(btn_exit);

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            backgroundTexture2D = Content.Load<Texture2D>("MenuBackground");
            matrixThemeSong = Content.Load<SoundEffect>("matrixTheme");
            engineSoundInstance = matrixThemeSong.CreateInstance();
            engineSoundInstance.Play();
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here


            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {

            GraphicsDevice.Clear(Color.Azure);

            // TODO: Add your drawing code here

            spriteBatch.Begin();
            spriteBatch.Draw(backgroundTexture2D, _rectangle, Color.White);
            spriteBatch.End();
            //GraphicsDevice.Clear(Color.CornflowerBlue);
            base.Draw(gameTime);
        }
    }
}
