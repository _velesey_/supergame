import os 
import shutil
from sys import platform as _platform



def deleteAllFiles(filepath):
	folder = filepath
	for the_file in os.listdir(folder):
	    file_path = os.path.join(folder, the_file)
	    try:
	        if os.path.isfile(file_path):
	            os.unlink(file_path)
	    except (Exception) as e:
	        print (e)

def copyAllFile(src, to):
	src_files = os.listdir(src)
	for file_name in src_files:
	    full_file_name = os.path.join(src, file_name)
	    if (os.path.isfile(full_file_name)):
	        shutil.copy2(full_file_name, to)

path = os.path.dirname(__file__)

beforeBuildLibsFolder = path + '/../Assemblies/_beforeBuildLibs'

srcFolder = ""
if _platform == "linux" or _platform == "linux2":
	srcFolder = "Linux"
elif _platform == "darwin":
    raise Exception("not found lib folder")
elif _platform == "win32":
	srcFolder = "WindowsGL"

srcFolder = path + '/../Assemblies/' + srcFolder

deleteAllFiles(beforeBuildLibsFolder)

copyAllFile(srcFolder, beforeBuildLibsFolder)
