﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperGame
{
    public class Group
    {
        Texture2D blockTexture;
        public int StartX;
        public int StartY;
        int blockSize;

		List<int[,]> arraList = new List<int[,]>();
		List<int[,]> template1List = new List<int[,]>();
		List<int[,]> template2List = new List<int[,]>();
		List<int[,]> template3List = new List<int[,]>();
		List<int[,]> template4List = new List<int[,]>();
		List<int[,]> template5List = new List<int[,]>();

        
		List<List<int[,]>> templates;

        public List<Block> blocks;

        public Group(Texture2D blockTexture, int startX, int startY, int blockSize)
        {
            this.blockTexture = blockTexture;
            this.StartX = startX;
            this.StartY = startY;
            this.blockSize = blockSize;
			templates = new List<List<int[,]>>();
     
            blocks = new List<Block>();

			// 0 - пусто
			// 1 - block
			//2 - death
			// 3 - footer
			arraList.Add(new int[6, 5] 
			{
				{0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0},
				{1, 0, 0, 0, 0},
				{1, 0, 0, 1, 0},
				{3, 0, 0, 3, 3}
			});
			////////////////////////////////////////////1
			template1List.Add(new int[6, 5] 
			                  {
				{0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0},
				{1, 0, 2, 0, 0},
				{3, 0, 3, 3, 3}
			});
			//2
			template1List.Add(new int[6, 5] 
			                  {
				{0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0},
				{0, 1, 1, 0, 0},
				{3, 3, 3, 3, 3}
			});
			//3
			template1List.Add(new int[6, 5] 
			                  {
				{0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 2, 0, 0},
				{3, 1, 2, 0, 0},
				{3, 3, 3, 3, 3}
			});
			//4
			template1List.Add(new int[6, 5] 
			                  {
				{0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 1, 0, 0},
				{1, 1, 0, 2, 0},
				{3, 3, 3, 3, 3}
			});

			/////////////////////////////////////////////5
			template2List.Add(new int[6, 5] 
			                  {
				{0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 1, 0, 0},
				{1, 1, 0, 2, 0},
				{3, 3, 3, 3, 3}
			});
			//6
			template2List.Add(new int[6, 5] 
			                  {
				{0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 2, 0, 0},
				{3, 2, 3, 3, 3}
			});
			//7
			template2List.Add(new int[6, 5] 
			                  {
				{0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0},
				{0, 1, 2, 0, 0},
				{1, 0, 2, 0, 0},
				{3, 3, 3, 3, 3}
			});
			//8

			/////////////////////////////////////////////
			template3List.Add(new int[6, 5] 
			                  {
				{0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0},
				{1, 1, 0, 2, 0},
				{3, 3, 0, 3, 3}
			});

			//9
			template3List.Add(new int[6, 5] 
			                  {
				{0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0},
				{0, 1, 0, 0, 0},
				{0, 1, 0, 0, 0},
				{3, 3, 0, 3, 3}
			});



			//////////////////////////////////////////////10
			template4List.Add(new int[6, 5] 
			{
				{0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 2, 0, 0},
				{1, 1, 2, 0, 0},
				{3, 3, 3, 3, 3},
			});
			//12
			template5List.Add(new int[6, 5] 
			{
				{0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0},
				{0, 1, 0, 0, 0},
				{1, 1, 0, 0, 1}
			});

			templates.Add(arraList);
			templates.Add(template1List);
			templates.Add(template2List);
			templates.Add(template3List);
			templates.Add(template4List);
			templates.Add(template5List);

			Reset(0);

        }

        public void Reset(int template)
        {
			int ar;
			Random r = new Random ();
			ar = r.Next(1, 5);
			template = ar;

            List<Block> temp = new List<Block>();
            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 5; j++)
                {
					Random rnd = new Random ();
					int rndI;
					if (templates [rnd.Next(1,5)].Count () > 0) {
						rndI = rnd.Next (0, templates [template].Count ());
					} else {
						rndI = 0;
					}
					int value = templates[template][rndI][i, j];
					if (value == 1)
					{

						int x = StartX + j * blockSize;
						int y = StartY + i * blockSize;
						Rectangle rect = new Rectangle(x, y, blockSize, blockSize);
						Block block = new Block(blockTexture, rect, Global.BlockType.platform);
						temp.Add(block);
					}
					if (value == 2)
					{
						int x = StartX + j * blockSize;
						int y = StartY + i * blockSize;
						Rectangle rect = new Rectangle(x, y, blockSize, blockSize);
						Block block = new Block(blockTexture, rect, Global.BlockType.death);
						temp.Add(block);
					}

					if (value == 3)
					{
						int x = StartX + j * blockSize;
						int y = StartY + i * blockSize;
						Rectangle rect = new Rectangle(x, y, blockSize, blockSize);
						Block block = new Block(blockTexture, rect, Global.BlockType.footer);
						temp.Add(block);
					}
                }
            }
            blocks.Clear();
            blocks.AddRange(temp);
        }

        public void Update(GameTime gametime)
        {

        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch, int dx, int dy)
        {
            StartX += dx;
            StartY += dy;
            foreach (var block in blocks) 
            {
                block.Draw(gameTime, spriteBatch, dx, dy);
            }
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            Draw(gameTime, spriteBatch, 0, 0);
        }
    }
}
